from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
#from kivy.uix.widget import Widget
from kivy.uix.label import Label
from kivy.uix.behaviors import ButtonBehavior
from kivy.properties import ObjectProperty


player = 0
coordinates_X = []
coordinates_O = []
who_played = None


class Area(ButtonBehavior, Label):
	"""Contem as Coordenadas das áreas"""
	coordinates = ObjectProperty()

	def __init__(self, **kwargs):
		super(Area, self).__init__(**kwargs)
		self.font_size = "80sp"

	def check_winner(self, who_played):
		def check_row(arg, pl):
			for col in [1 ,2 ,3]:
				if arg.count(col) == 3:
					print(pl, 'Venceu')
					c=RootApp()
					c.remove_widget(self)

		def check_col(arg, pl):
			for row in [1 ,2 ,3]:
				if arg.count(row) == 3:
					print(pl, 'Venceu')

		def check_diag(arg, pl):
			if (1,1,) in arg and (2,2,) in arg and (3,3,) in arg:
				print(pl, 'Venceu')
			elif (1, 3,) in arg and (2, 2,) in arg and (3, 1,) in arg:
				print(pl, 'Venceu')

		def _check():
			if who_played == 'X':
				coord_list_x = []
				coord_list_y = []
				for c_x, c_y in coordinates_X:
					coord_list_x.append(c_x)	
					coord_list_y.append(c_y)
				check_col(coord_list_x, 'X')
				check_row(coord_list_y, 'X')
				check_diag(coordinates_X, 'X')
			elif who_played == 'O':
				coord_list_x = []
				coord_list_y = []
				for c_x, c_y in coordinates_O:
					coord_list_x.append(c_x)	
					coord_list_y.append(c_y)
				check_col(coord_list_x, 'O')
				check_row(coord_list_y, 'O')
				check_diag(coordinates_O, 'O')

		_check()
		
	def on_press(self):
		global player
		if not self.text:
			if not player:
				self.text = 'X'
				player = 1
				coordinates_X.append(self.coordinates)
				who_played = 'X'
				self.check_winner(who_played)
			elif player:
				self.text = 'O'
				player = 0
				coordinates_O.append(self.coordinates)
				who_played = 'O'
				self.check_winner(who_played)
		

class RootApp(BoxLayout):
	"""Tela principal"""
	pass


class HashApp(App):
	"""Classe principal"""
	pass


if __name__ == '__main__':
	HashApp().run()